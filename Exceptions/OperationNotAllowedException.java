package Exceptions;

public class OperationNotAllowedException extends BankException{

    public OperationNotAllowedException(String operation){
        super("Operation is not allowed: " + operation);
    }



}
