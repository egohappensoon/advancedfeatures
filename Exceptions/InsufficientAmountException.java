package Exceptions;

public class InsufficientAmountException extends BankException {

    public InsufficientAmountException(String message) {
        super(message);
    }
}
