package IO.src.sda;
import IO.src.sda.*;
import com.sun.source.tree.WhileLoopTree;

import java.awt.event.MouseWheelEvent;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class IOMain {


    public IOMain() throws IOException {
    }

    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {


        Person person = new Person("Teet Tee", 28,"tel123","tel456","tel789");
        System.out.println(person);

        int personAge = person.getAge();
        System.out.println("age " + personAge);

        personAge = 50;
        System.out.println("Modified age var " + personAge);
        System.out.println("Person age is still " + person.getAge());

        List<String> personNumbers = new ArrayList<>(person.getPhoneNumbers());
        System.out.println("Nums "+ personNumbers);

        personNumbers.add("Tel New Number");
        System.out.println("Modified numbers var "+ personNumbers);
        System.out.println(person);

        // Collections.
        // Helpers.
        // Arrays.
        // Objects.


        /**
         * Reading data from files and storing data to file
         **/


        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;

        try {
            // inputStream = new FileInputStream("cat.jpg");
            inputStream = new FileInputStream("IO/src/sda/cat.jpg");
            outputStream = new FileOutputStream("IO/src/sda/catCopy.jpg");

            int c = inputStream.read();
            while(c != -1) {
                outputStream.write(c);
                c = inputStream.read();
            }
        } finally {
            if(inputStream != null){
                inputStream.close();
            }if(outputStream != null){
                    outputStream.close();
            }
        }

        FileReader inputReader = null;
        FileWriter outputWriter = null;
        try{
            inputReader = new FileReader("IO/src/sda/input.txt");
            outputWriter = new FileWriter("IO/src/sda/output.txt");
            int c = inputReader.read();
            while(c != -1) {
                outputWriter.write(c);
                System.out.println((char) c); // write each character
                c = inputReader.read();
//
//                if (c ==1234) {
//                    c = outputWriter.write();
//                }
            }
        }finally {
            if(inputReader != null){
                inputReader.close();
            }if(outputWriter != null){
                outputWriter.close();
            }
        }



        //BufferedReader (String Reader) will read texts line by line and print them out line by line (as is)
        BufferedReader bufferedReader = null;

        try {
            bufferedReader = new BufferedReader(new FileReader("IO/src/sda/input.txt"));
            String line = bufferedReader.readLine();
            while (line != null){
                System.out.println(line);
                line = bufferedReader.readLine();
            }
        } finally {
            if(bufferedReader != null){
                bufferedReader.close();
            }
        }


        //Using try() like this eliminates the calling of close() function, the method closes itself when done

        List<String> linesFromFile = new ArrayList<>();
        try (
           BufferedReader bufferedReader1 = new BufferedReader(new FileReader("IO/src/sda/input.txt"));
        ){
            String line = bufferedReader1.readLine();
            while (line != null){
                linesFromFile.add(line);
                System.out.println(line);
                line = bufferedReader1.readLine();
            }
        }

        try (
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("IO/src/sda/output2.txt"))
        ){
            for (String s : linesFromFile){
                bufferedWriter.write(s);
                bufferedWriter.flush(); // W,rite data to file and clean the buffer (flush())
            }
        }

        //Using Scanner function to read from text file
        Scanner scanner = new Scanner(new BufferedReader(new FileReader("IO/src/sda/input2.txt")));
        System.out.println();
        while (scanner.hasNextInt()){
            int number = scanner.nextInt();
            System.out.println("Number from file: "+ number);
        }

//        scanner.nextInt();  //Cant read int line and next String, it must be defined before it will work well
//        scanner.nextLine();

       /** int n = scanner.nextInt();
        //scanner.nextLine();
        //scanner.nextLine();

        double d = scanner.nextByte();

        String fullLine = scanner.nextLine();
        int fromLine = Integer.parseInt(fullLine);

        try{
            double doubleFromLine = Double.parseDouble(fullLine);
        }catch (NumberFormatException e){
            System.out.println("");
        }*/

        BusinessCard businessCard1 = new BusinessCard("Tilt Tee", "1793791771", "tilt@tilt.ee");
        System.out.println();
        try(
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("IO/src/sda/businessCard.txt"))
            ){
            bufferedWriter.write(businessCard1.getName());
            bufferedWriter.newLine();
            bufferedWriter.write(businessCard1.getPhoneNumber());
            bufferedWriter.newLine();
            bufferedWriter.write(businessCard1.getEmail());
            bufferedWriter.newLine();
        }
        BusinessCard businessCard2 = null;
        try(
            BufferedReader bufferedReader1 = new BufferedReader(new FileReader("IO/src/sda/businessCard.txt"))
            ) {
            String name = bufferedReader1.readLine();
            String phoneNumber = bufferedReader1.readLine();
            String email = bufferedReader1.readLine();
            businessCard2 = new BusinessCard(name, phoneNumber, email);
        }
        System.out.println(businessCard2);


        //Read BusinessCards.txt into an ArrayList<> or a type of Collections
        List<BusinessCard> businessCards1 = new ArrayList<>();
        System.out.println();
        BusinessCard businessCardTEMP = null;
        try(
                BufferedReader bufferedReaderTOcollections = new BufferedReader(new FileReader("IO/src/sda/businessCards.txt"))
                ){
            while(true){
                String name = bufferedReaderTOcollections.readLine();
                String phoneNumber = bufferedReaderTOcollections.readLine();
                String email = bufferedReaderTOcollections.readLine();
                if(name == null|| phoneNumber == null || email == null){
                    break;
                }
                businessCardTEMP = new BusinessCard(name, phoneNumber, email);
                businessCards1.add(businessCardTEMP);
            }
            System.out.println(businessCards1);
        }


        //SERAILIZED texts writing...
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("IO/src/sda/businessCardsSerialized.txt"));
        objectOutputStream.writeObject(businessCards1);
        objectOutputStream.close();

        ObjectInputStream objectInputStream1 = new ObjectInputStream(new FileInputStream("IO/src/sda/businessCardsSerialized.txt"));
        List<BusinessCard> bc = (List<BusinessCard>) objectInputStream1.readObject();
        System.out.println(bc);
        objectInputStream1.close();

        // java.nio;

        Path path = Paths.get("IO/src/sda/input.txt");
        Path absolutePath = Paths.get("C\\Users");
        absolutePath = Paths.get("tmp/anotherFolder/yetAnotherFolder");
        path.getParent();
        path.getRoot();

        /*List<String> allLines = Files.readAllLines(path);
        // Files.  ///play with it at home...
        *//**
         * 1)Read file nonsense.txt 2)Count total number of words  3)Count number of each word
         * **//*

        //Read Nonsense.txt and do the above mentioned tasks
        List<BusinessCard> nonsenseText = new ArrayList<>();
        System.out.println();
        BusinessCard nonsenseTextTEMP = null;
        try(
                BufferedReader bufferedReaderNON = new BufferedReader(new FileReader("IO/src/sda/nonsense.txt"))
        ){
                String words = bufferedReaderNON.readLine();
                int wordCount = 0;
            while(words != null){
                String[] word = words.split(" "); // split each word using a space or "\\s"
                wordCount = wordCount + word.length;
            }
            bufferedReaderNON.close();
            System.out.println("Number of words in this file: " + wordCount);
        }*/

        Path nonsenseFile = Paths.get ("IO/src/sda/nonsense.txt");
        List<String> allLines = Files.readAllLines(nonsenseFile);
/*
        for (String line : allLines) {
            System.out.println(line);
            String[] words = line.split("\\s");
            System.out.println("Words in a line: " + words.length);
            totalWords += (words.length - 1);

            for (String word : words) {
                word = word.replaceAll("\\W", " ")
                int currentNumberOfWords = wordCounter.getOrDefault(word, 0);
                wordCounter.put(word, currentNumberOfWords + 1)
            }
            System.out.println(TTotal words:"" + totalWords);

            for (Map.Entry<String, Integer> wordCountPair : wordCounter.entrySet()) {
                System.out.println("Word: " + wordCountPair.getKey() + " - " + wordCountPair.getValue() + "times");
            }
        }
*/



    }




}
