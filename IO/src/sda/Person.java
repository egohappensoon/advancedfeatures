package IO.src.sda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Person {

    private String name;
    private int age;
    private List<String> phoneNumbers = new ArrayList<>();


    public Person(String name, int age, String... phoneNumbers) {
        this.name = name;
        this.age = age;

        for (String phoneNumber : phoneNumbers) {
            this.phoneNumbers.add(phoneNumber);
        }
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getPhoneNumbers() {
        //return phoneNumbers;
        return Collections.unmodifiableList(phoneNumbers); //This makes sure the collection can't be modified, throws error
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", phoneNumbers=" + phoneNumbers +
                '}';
    }


}
