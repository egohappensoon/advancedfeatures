package Interfaces.src.com;

public interface Swimmer {

    int DEFAULT_SPEED= 20;

    void swim();

    default double getSwimmingSpeed(){
        return DEFAULT_SPEED;
    }


}
