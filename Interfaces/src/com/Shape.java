package Interfaces.src.com;

public interface Shape {

    double getWidth();
    double getLength();
    double getArea();


}
