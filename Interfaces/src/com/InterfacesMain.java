package Interfaces.src.com;

import com.sun.source.tree.WhileLoopTree;

import java.util.*;

public class InterfacesMain {

    public static void main(String[] args) {

        Shape[] shapes = new Shape[10];

        for (int i= 0; i < shapes.length; i++){
            Shape shape;
            if (i % 2 == 0) {
                 shape = new WrapperBox();
            }else {
                Random random = new Random();
                shape = new Book(random.nextInt(10+1),random.nextInt(20+1));
            }
            shapes[i] = shape;
        }
        for (Shape shape : shapes){
            System.out.println("Shape types: " + shape.getClass()+ " width: " + shape.getWidth()+" length: " + shape.getLength()+" area: " +shape.getArea());
        }


        double totalWidth = 0;
        double totalLength = 0;
        double totalUsedArea = 0;
        for (Shape shape : shapes){
            totalLength += shape.getLength();
            totalWidth += Math.max(totalWidth, shape.getWidth());
            totalUsedArea = shape.getArea();
        }

        double totalAreaOfContainer = totalLength * totalWidth;
        double effectivelyUsedRatio = totalUsedArea / totalAreaOfContainer;

        System.out.println("Total width: " + totalWidth + " total length: " + totalLength);

        Person person = new Person();
        person.swim();
        person.meow();

        Cat cat = new Cat();
        cat.meow();

        //Get the area of the box
        Box boxWithBook = new Box();
        boxWithBook.setItem(new Book(14, 20));
        Book book = (Book) boxWithBook.getItem();
        System.out.println("Area of the book is: " + book.getArea());

        //Get the area of a new box
        Box boxWithBook2 = new Box();
        boxWithBook2.setItem(new Book(4, 2));
        Book bookFromTheBox = (Book) boxWithBook2.getItem();
        System.out.println("Area of second book " + bookFromTheBox.getArea());

        Box boxWithCat = new Box();
        boxWithCat.setItem(cat);
        Cat catFromTheBox = (Cat) boxWithCat.getItem();
        catFromTheBox.meow();

        Box[] cargo = new Box[3];
        cargo[0] = boxWithBook;
        cargo[1] = boxWithBook2;
        cargo[2] = boxWithCat;

        for (Box currentBox : cargo){
            Object itemInTheBox = currentBox.getItem();

            if (itemInTheBox instanceof Book) {
                Book b = (Book) itemInTheBox;
                System.out.println("Book area: " + b.getArea());
            }else if(itemInTheBox instanceof Cat){
                Cat c = (Cat) itemInTheBox;
                c.meow();
            }
        }

        //
        GenericBox<Book> genericBoxForBook = new GenericBox<>();
        genericBoxForBook.setItem(new Book(3, 7));
        bookFromTheBox = genericBoxForBook.getItem();
        System.out.println("Area of the book: "+ bookFromTheBox.getArea());

        //
        GenericBox<Cat> genericBoxForCat = new GenericBox<>();
        genericBoxForCat.setItem(new Cat());
        catFromTheBox = genericBoxForCat.getItem();
        catFromTheBox.meow();

        GenericBoxFor2Items<Book,Cat> genericBoxFor2Items = new GenericBoxFor2Items<>();
        genericBoxFor2Items.setItem(new Book(2,4));
        catFromTheBox = genericBoxForCat.getItem();
        catFromTheBox.meow();

        System.out.println("-----------");
        System.out.println("Book area: "+ bookFromTheBox.getArea());
        catFromTheBox.meow();


        //Pt anything here where the '?' is
        GenericBox<?> boxWithAnything = new GenericBox<>();

        //Upper boundary
        GenericBox<? super Book> boxWithParentsOfBook = new GenericBox<>();


        //ARRAYLISTS
        ArrayList<String> shoppingList = new ArrayList<>();

        shoppingList.add("Milk");
        shoppingList.add("Bread");
        shoppingList.add("Cheese");
        shoppingList.add("Ham");
        System.out.println("Shopping list: " + "total items are " + shoppingList.size());
        System.out.println(shoppingList.get(3));

        Iterator<String> shoppingListIterator = shoppingList.iterator();
        while (shoppingListIterator.hasNext()){
            String shoppingItem = shoppingListIterator.next();
            System.out.println("Item: "+ shoppingItem);
        }

        for (String shoppingItem : shoppingList){
            System.out.println("For-each loop item: "+ shoppingItem);
        }

        //remove some items
        shoppingList.remove(3);
        shoppingList.remove("Milk");

        for ( String shoppingItem : shoppingList){
            System.out.println("Left to buy: "+shoppingItem);
        }

        //CHeck the list if they contain items using contains and indexOf keywords
        System.out.println("Need to buy water? "+ shoppingList.contains("Water"));
        System.out.println("Need to buy bread? "+ (shoppingList.indexOf("Bread") >= 0));
        System.out.println("Need to buy water? "+ (shoppingList.indexOf("Water") >= 0));


        //Create a TO-DO list
        ArrayList<String> toDoList = new ArrayList<>();
        System.out.println();

        toDoList.add("Open laptop");
        toDoList.add("push power button");
        toDoList.add("Wait for 5 seconds");
        toDoList.add("Enter password");
        toDoList.add("Click on web browser");
        toDoList.add("Type in the URl");
        toDoList.add("Read the article");
        toDoList.add("Close the browser");
        toDoList.add("Shutdown the computer");
        toDoList.add("Close laptop");

        for (String toDoItems : toDoList){
            System.out.println("ToDo items: "+toDoItems);
        }

        //Remove item
        toDoList.remove("Wait for 5 seconds");

        for (String toDoItems : toDoList){
            System.out.println("ToDo items: "+toDoItems);
        }

        //Checking how long it takes to run in LinkedList vs ArrayList
        LinkedList<String> testLinkedList = new LinkedList<>();
        System.out.println();
        long startTime = System.currentTimeMillis();
        for (int i = 0; i<10000; i++){
            testLinkedList.add("Some string: ");
        }
        long finishTime = System.currentTimeMillis();
        System.out.println("LinkedList add totaltime: "+ (finishTime - startTime));

        ArrayList<String> testArrayList = new ArrayList<>();
        startTime = System.currentTimeMillis();
        for (int i = 0; i<10000; i++){
            testArrayList.add("Some string: ");
        }
        finishTime = System.currentTimeMillis();
        System.out.println("ArrayList add totaltime: "+ (finishTime - startTime));


        //Get the values of items using both LinkedList and ArrayList
        LinkedList<String> testLinkedListGET = new LinkedList<>();
        System.out.println();
        long startTimes = System.currentTimeMillis();
        for (int i=testLinkedListGET.size()-1;i >= 0; i--){
            testLinkedListGET.remove(i);
        }
        long finishTimes = System.currentTimeMillis();
        System.out.println("LinkedList remove totaltime: "+ (finishTimes - startTimes));

        ArrayList<String> testArrayListGET = new ArrayList<>();
        startTimes = System.currentTimeMillis();
        for (int i=testArrayListGET.size()-1;i >= 0; i--){
            testArrayListGET.remove(i);
        }
        finishTimes = System.currentTimeMillis();
        System.out.println("LinkedList remove totaltime: "+ (finishTimes - startTimes));


        LinkedList<String> waitingList = new LinkedList<>();
        System.out.println();
        for (int i= 0; i< 5; i++){
            waitingList.addLast("Person: "+ i);
        }
        while(!waitingList.isEmpty()){
            String personToServe = waitingList.poll();
            System.out.println(personToServe + " is served!");
        }


        //QUEUES....PriorityQueue
        PriorityQueue<Person> personPriorityQueue = new PriorityQueue<>(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                boolean firstPensioner = o1.getAge() > 65;
                boolean secondPensioner = o2.getAge() > 65;
                if (firstPensioner && !secondPensioner){
                    return -1;
                }
                if (!firstPensioner && secondPensioner){
                    return 1;
                }
                return 0;
            }
        });
        personPriorityQueue.add(new Person(30));
        personPriorityQueue.add(new Person(70));
        personPriorityQueue.add(new Person(15));
        personPriorityQueue.add(new Person(73));
        personPriorityQueue.add(new Person(20));
        personPriorityQueue.add(new Person(40));

        while(!personPriorityQueue.isEmpty()){
            Person servicedPerson = personPriorityQueue.poll();
            System.out.println("Age of Serviced Persons: "+ servicedPerson.getAge());
        }

        //SETS...unordered type in collection
        HashSet<String> visitedCities = new HashSet<>();
        visitedCities.add("Tartu");
        visitedCities.add("Tallinn");
        visitedCities.add("Rapla");
        visitedCities.add("Tartu");
        visitedCities.add("Tallin");
        visitedCities.add("Narva");

        for (String city : visitedCities){
            System.out.println("I visited: "+ city + " one day.");
        }
        visitedCities.remove("Tartu");
        visitedCities.remove("Viljandi");
        System.out.println("After removal...");
        for (String city : visitedCities){

            System.out.println("I visited: "+ city + " one day.");
        }

        Book book1 = new Book(12, 14);
        Book book2 = new Book(25, 22);
        System.out.println("Equals? " + book1.equals(book2));

        Book book3 = new Book(12, 14);
        System.out.println("Equals 1 & 3? "+ book1.equals(book3));


        //Set..HashSet
        Set<Book> readBooks = new HashSet<>();
        readBooks.add(book1);
        readBooks.add(book2);
        readBooks.add(book3);
        for (Book readBook : readBooks){
            System.out.println("Read book: "+readBook);
        }

        //HASHMAPS...Maps
        HashMap<Book,String> bookAuthorsMap = new HashMap<>();
        bookAuthorsMap.put(new Book(1,2), "Author A");
        bookAuthorsMap.put(new Book(3,4), "Author B");
        bookAuthorsMap.put(new Book(4,3), "Author A");
        bookAuthorsMap.put(new Book(11,22), "Author C");

        for (Book bookFromMap : bookAuthorsMap.keySet()){
            System.out.println("Book from Map: "+ bookFromMap);
        }
        for (String authorFromMap : bookAuthorsMap.values()){
            System.out.println("Author from Map: "+ authorFromMap);
        }
        for (Map.Entry<Book, String> bookAuthor : bookAuthorsMap.entrySet()){
            System.out.println("Mapping book: "+ bookAuthor.getKey() + "Author: " + bookAuthor.getValue());
        }

        System.out.println("Author of 1x2 is: "+bookAuthorsMap.get(new Book(1,3)));
        System.out.println("Author of 10x20 is: "+bookAuthorsMap.get(new Book(10,20)));
        System.out.println("Author of 10x20 is: "+bookAuthorsMap.getOrDefault(new Book(10,20), "Unknown"));





    }
}
