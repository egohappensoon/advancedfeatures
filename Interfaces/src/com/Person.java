package Interfaces.src.com;

public class Person implements Swimmer, Meoable {


    private int age;

    public Person() {
    }

    public Person(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public void meow() {
        System.out.println("I am meowing");
    }

    @Override
    public void swim() {
        System.out.println("I am swimming");
    }

    @Override
    public double getSwimmingSpeed() {
       return 40;
    }

    @Override
    public void person() {

    }

}
