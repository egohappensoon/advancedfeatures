package Interfaces.src.com;

import java.util.Objects;

public class Book implements Shape, Comparable<Book> {

    private int width;
    private int length;


    public Book(int width, int length) {
        this.width = width;
        this.length = length;
    }

    @Override
    public double getWidth() {
        return 0;
    }

    @Override
    public double getLength() {
        return 0;
    }

    @Override
    public double getArea() {
        return 0;
    }


    @Override
    public int compareTo(Book otherBook) {
        return (int) (getArea() - otherBook.getArea());
    }

    @Override
    public boolean equals(Object o) {
        // Please edit this to the one in class
        if (o instanceof Book){
            Book book = (Book) o;
            return width == book.width && length == book.length;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(width, length);
    }

    @Override
    public String toString() {
        return "Book{" +
                "width=" + width +
                ", length=" + length +
                '}';
    }
}
