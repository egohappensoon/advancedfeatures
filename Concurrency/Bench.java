package Concurrency;

public class Bench {

    private int seatAvailable;

    public Bench(int seatAvailable) {
        this.seatAvailable = seatAvailable;
    }

    public synchronized void takeSeat(){ // Synchronized - only one thread can call this method at any given time
        if (seatAvailable > 0){
            System.out.println("Seats available: " + seatAvailable);
            seatAvailable--;
            System.out.println("Seats left: " + seatAvailable);
        }else {
            System.out.println("No seat available :(");
        }

        /**synchronized (this){
        if (seatAvailable > 0){
            System.out.println("Seats available: " + seatAvailable);
            seatAvailable--;
            System.out.println("Seats left: " + seatAvailable);
        }else {
            System.out.println("No seat available :("); }*/

    }


}
