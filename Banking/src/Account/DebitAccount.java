package Account;

import Currency.Currency;

import javax.naming.OperationNotSupportedException;

public class DebitAccount {


    private String owner;
    private AccountStatus status;
    private double amount;
    private Currency currency = Currency.EUR;


    public DebitAccount(String owner) {
        this.owner = owner;
        this.status = AccountStatus.OPEN;
    }

    public double getAmount() throws OperationNotSupportedException {
        if (status.canSeeAmount){
           return amount;
        }
        throw new OperationNotSupportedException("You aren't allowed to see the amount");
        //return 0;
    }

    public DebitAccount(String owner, Currency currency) {
        this(owner); // calls the
        this.currency = currency;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    public void withdraw(double amountToWithdraw) throws OperationNotSupportedException {
       // amount = amount - amountToWithdraw;
        if (status.canWithdraw){
            if (amountToWithdraw > amount)
            throw new OperationNotSupportedException("You don't have sufficient amount.");
        }else{
            amount -= amountToWithdraw;
        }
    }

    public void withdraw(double amountToWithdraw, Currency currency) throws OperationNotSupportedException {
       // TODO -- convert currency to local account currency before withdrawing
            double convertedAmountToWithdraw = currency.convertToCurrency(amountToWithdraw, currency);
            withdraw(convertedAmountToWithdraw);
    }

    public void deposite(double amountToDeposite){
        if (status.canDeposit){
            amount += amountToDeposite;
        }
    }

    public void deposite(double amountToWithdraw, Currency currency) throws OperationNotSupportedException {
        // TODO  -- convert currency to local account currency before deposting
                   double convertedAmountToDeposite = currency.convertToCurrency(amountToWithdraw, currency);
                   withdraw(convertedAmountToDeposite);
    }

    public void pay(double amountToPay){
        if (status.canWithdraw){
            amount -= amountToPay;
        }
    }

    public void pay(double amountToWithdraw, Currency currency) throws OperationNotSupportedException {
        // TODO -- convert currency to local account currency before paying
        double convertedAmountToPay = currency.convertToCurrency(amountToWithdraw, currency);
        withdraw(convertedAmountToPay);

    }

    public void transfer(double amountToTransfer, DebitAccount account){
        if (status.canWithdraw && account.getStatus().canDeposit){
            amount -= amountToTransfer;
            double exchangedAmount = currency.convertToCurrency(amountToTransfer, account.currency);
            account.deposite(exchangedAmount);

        }
    }



}
