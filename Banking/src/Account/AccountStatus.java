package Account;

import Account.DebitAccount;

public enum AccountStatus {
        OPEN(true,true, true),
        LOCKED(false, false, false),
        CLOSED();
        //CLOSED(false, false, false);


    boolean canSeeAmount;
    boolean canDeposit;
    boolean canWithdraw;



    AccountStatus(boolean canSeeAmount, boolean canDeposit, boolean canWithdraw) {
        this.canSeeAmount = canSeeAmount;
        this.canDeposit = canDeposit;
        this.canWithdraw = canWithdraw;
    }

    AccountStatus(){

    }

    public boolean isCanSeeAmount() {
        return canSeeAmount;
    }

    public boolean isCanDeposit() {
        return canDeposit;
    }

    public boolean isCanWithdraw() {
        return canWithdraw;
    }


    @Override
    public String toString() {
        return "DebitAccount.AccountStatus{" +
                "canSeeAmount=" + canSeeAmount +
                ", canDeposit=" + canDeposit +
                ", canWithdraw=" + canWithdraw +
                '}';
    }


}
