package Currency;

import Account.AccountStatus;
import Account.DebitAccount;

import javax.naming.OperationNotSupportedException;


public class BankingMain {

    public static void main(String[] args) throws Exception {
        //Call the currency class to be used here
        Currency currency = Currency.EUR;
        System.out.println(currency);

        System.out.println("10 " + currency);

        double rublesAmount = 1000;
        double eurosAmount = Currency.RUB.convertToEuros(rublesAmount);
        System.out.println(rublesAmount + " " + Currency.RUB + " = " + eurosAmount + " " + Currency.EUR + "\n");


        Currency[]  allCurrencies = Currency.values();

        for (Currency c : allCurrencies){
            System.out.println("Available currency: " + c);
        }


        // Currency.Currency currentCurrency = allCurrencies;

        for ( Currency d : allCurrencies) {
            //System.out.println("Eesti kroon is not in use anymore, use Euros. ");
            switch (d) {
                case ESK:
                case DAK:
                case PLN:
                    System.out.println(d + " is not in use anymore.");
                    break;
                default:
                    System.out.println(d + " is in use"+ "\n");
            }

            // Currency currentCurrency = Currency.ESK;

            /**if (currentCurrency == Currency.ESK || currentCurrency == Currency.DAK || currentCurrency == Currency.PLN) {
             System.out.println("Your currency is not in use anymore, use Euros.");
             }*/
        }

        //Dealing with AccountStatus class
        //Calling the class here
        AccountStatus[] accountStatus = AccountStatus.values();

        for (AccountStatus status : accountStatus){
            System.out.println("All status are: " + status);

            System.out.println(status.isCanDeposit()+ "\n");
            // System.out.println(status.isCanDeposit);
            //System.out.println(status.isCanWithdraw);
        }


        //Dealing with DebitAccount class

        DebitAccount debitAccount = new DebitAccount("John Doe" );
        debitAccount.deposite(1000);
        try {
            debitAccount.withdraw(1500);
        } catch (OperationNotSupportedException e) {
            System.out.println("Not enough money! " + e.getMessage());
        }
        debitAccount.pay(50);

        //Get the amount of money left and the account status
        System.out.println("Money left: " + debitAccount.getAmount());
        System.out.println("Account status: " + debitAccount.getStatus()+ "\n");

        debitAccount.setStatus(AccountStatus.LOCKED);

        debitAccount.pay(200);
        try {
            System.out.println("Money left: " + debitAccount.getAmount());
        } catch (OperationNotSupportedException e) {
            System.out.println("Something went wrong: " + e.getMessage());
        }


        DebitAccount friendsAccount = new DebitAccount("Jane Doe" );
        debitAccount.setStatus(AccountStatus.OPEN);
        debitAccount.transfer(100, friendsAccount);
        debitAccount.pay(50);

        //Get the amount of money left in my account and friends account as well
        System.out.println("Money left in my account: " + debitAccount.getAmount());
        System.out.println("Money left in friend's account: " + friendsAccount.getAmount()+ "\n");


        // Set the account status to close and check the account balance
        friendsAccount.setStatus(AccountStatus.CLOSED);
        try {
            System.out.println("Money left in friend's account: "+ friendsAccount.getAmount());
            System.out.println("Operation 1 ");
            System.out.println("Operation 2 ");
            System.out.println("Operation 3 ");
        } catch (OperationNotSupportedException e) {
            System.out.println("Something went wrong: " + e.getMessage());
        }


    }
}
