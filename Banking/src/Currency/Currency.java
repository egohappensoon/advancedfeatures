package Currency;

public enum Currency {
    EUR("Euro", 1),
    USD("US dollar", 0.8),
    RUB("Russian ruble", 0.003),
    PLN("Polish zloty", 0.25),
    YEN("Japanese yen", 0.32),
    DAK("Danish kroon", 0.24),
    ESK("Eesti kroon", 0.07);

    double exchangeRateToEuro;

    String name;

    Currency(String name, double exchangeRateToEuro){
        this.exchangeRateToEuro = exchangeRateToEuro;
        this.name = name;
    }


    public double convertToEuros(double amount){
        return amount * exchangeRateToEuro;
    }

    public double convertToCurrency(double amount, Currency currency){
        double eurosAmount = convertToEuros(amount);
        return eurosAmount / currency.exchangeRateToEuro;
    }

    @Override
    public String toString() {
        return name;
    }
}
